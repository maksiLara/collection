package com.maksimLarchanka;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<String> family = new LinkedList<>();
        family.add("Мама");
        family.add("Папа");
        family.add("Я");

        List<String> name = new LinkedList<>();
        name.add("Счастливая семья");
        name.add("Алла");
        name.add("Игорь");
        name.add("Максим");

        ListIterator<String> familyIter = family.listIterator();
        ListIterator<String> nameIter = name.listIterator();
        while (nameIter.hasNext()) {
            if (familyIter.hasNext()) familyIter.next();
            familyIter.add(nameIter.next());
            System.out.println(family);
        }
        nameIter = (ListIterator<String>) name.iterator();
        while (nameIter.hasNext()) {
            nameIter.next();
            if (nameIter.hasNext()) {
                nameIter.next();
                nameIter.remove();
            }
        }
        System.out.println(name);
        family.removeAll(name);
        System.out.println(family);

    }
}
